---
title: "Funzioni in c++"
date: 2019-04-13T17:20:32+02:00
draft: true
---

Le funzioni sono blocchi di programma indipendenti da altri moduli (per esempio la funzione main), ciascuno destinato ad una precisa operazione.
```cpp
<tipo> <nome_funzione> (<tipo> <nome_arg1>, <tipo> <nome_arg2>, ... <tipo> <nome_argN>);
```
La prima parte della dichiarazione indica il tipo della funzione che puo' essere: 
<ul>
<li>int che dara' output un numero intero;</li>
<li>float che dara' output un numero reale;</li>
<li>double che dara' output numero reale 'lungo' esso ha infatti a disposizione 64 bit, contro i 32 di float;</li>
<li>char che dara' in output un qualsiasi carattere definito secondo lo standard ASCII;</li>
<li>void che non dara' nessun output.</li></ul>
 Esso e' seguito dal nome della funzione e da una lista di parametri tra parentesi tonde, separati da virgole. Per ognuno di essi si indica il tipo ed il nome. La dichiarazione e' terminata dal simbolo ';'
<h1>Esempio funzione 'somma'</h1> 
```cpp
#include <iostream>
using namespace std;
int somma(int a, int b){
int c;
c=a+b;
return c;
}

int main()
{
int primonumero, secondonumero;
cin>> primonumero>> secondonumero;
cout<<"la somma dei due numeri e' "<<somma(primonumero,secondonumero)<<endl;
return 0;
}

```
<h1>Funzioni VOID</h1>
Le funzioni void sono un particolare tipo di funzioni che non restituiscono alcun tipo di valore. 

```cpp
<tipo> <nome_funzione> (<tipo> <nome_arg1>, <tipo> <nome_arg2>, ... <tipo> <nome_argN>);
```


L'elenco dei parametri segue la sintassi della dichiarazione della variabile, i parametri sono separati dalla virgola e per ciascun parametro deve essere indicato il tipo.
<h1>Esempio funzione 'somma-void'</h1>

```cpp 
#include <iostream>
using namespace std;
int somma(int a, int b){
int c;
c=a+b;
cout<<c;
}

int main()
{
int primonumero, secondonumero;
cin>> primonumero>> secondonumero;
somma(primonumero,secondonumero);
return 0;
}

```

<h1>ESERCIZI</h1>
<h2>calcolo delta in una funzione di secondo grado</h2>

```cpp
#include <iostream>
#include <cmath>
using namespace std;

void calcolodelta (float a,float b,float c){
float delta;
delta=b*b-4*a*c;
cout<<delta;

}


int main()
{
double a, b, c;
cout<<"inserisci i valori a,b,c dell'equazione ax^2+bx+c=0 ";
cin>>a;
cin>>b;
cin>>c;

calcolodelta(a,b,c);
}

```

<h2>calcolo funzione di secondo grado</h2>
per poter calcolare la funzione di secondo grado posso semplicemente copiare la funzione calcolodelta dal programma
 precedente e aggiungere una piccola parte:


```cpp

#include <iostream>
#include <cmath>
using namespace std;
float calcolodelta (float a, float b, float c)
{
	float delta;
	delta=b*b-4*a*c;
	return delta;
}
	void risolutore_equazione_2_grado (float a, float b, float c){
	double delta, x1, x2;
	x1=(-b+sqrt(calcolodelta(a,b,c)))/2*a;
	x2=(-b-sqrt(calcolodelta(a,b,c)))/2*a;
	cout<<"x1= "<<x1<<" x2= "<<x2<<endl;
}
int main()
{
	double a, b, c;
	cout<<"inserisci i valori a,b,c dell'equazione ax^2+bx+c=0 ";
	cin>>a;
	cin>>b;
	cin>>c;
	risolutore_equazione_2_grado(a, b, c);
}
```

<h1>Passaggio di parametri per valore o per riferimento</h1>


L'operazione, con la quale la funzione main invia i valoria lla funzione, assegnandoli ai parametri, si chiama PASSAGGIO DI PARAMETRI.


Il concetto matematico di funzione definisce un'entita' astratta che associa valori di ingresso a valori d'uscita.
 Ma da un punto di vista teorico, una funzione definisce soltanto una relazione tra queste istanze, e non ne altera 
il valore.

Nell'ambito della programmazione,
 il costrutto di funzione rispecchia questa dinamica di base,
 nel senso che non altera i parametri che le vengono dati. L'esempio seguente ne e' una prova:

```cpp

#include <iostream>
void swap(int a, int b)
{
	int tmp = a;
	a = b;
	b = tmp;
}
int main()
{
	int x = 1;
	int y = 2;
	swap(x, y);
	cout << "x: " << x;
	cout << "y: " << y;
	return 0;

```

La funzione swap, che dovrebbe scambiare i valori dei suoi parametri, in realta' non produce alcun effetto
 apprezzabile all'esterno.

Il motivo risiede nel fatto che le istruzioni contenute all'interno di una funzione operano su una 
copia dei parametri in ingresso. All'interno della funzione swap, quindi, a e b sono copie rispettivamente 
di x e y, che sopravvivono solo fino al termine della funzione. Per questo motivo, cambiarne il valore tramite
 un'operazione di assegnamento, come avviene nella nostra funzione, non produce alcun effetto nel contesto del 
chiamante dove risiedono x e y.

Questo meccanismo di base e' noto come passaggio per valore dei parametri.
Copiare un parametro in ingresso ad una funzione, sebbene sia penalizzante da un punto di vista delle risorse 
(uso di memoria, cicli di processore, eccetera), e' utile perche' protegge i parametri di ingresso da modifiche
 accidentali, dette effetti collaterali, che potrebbero invalidare l'esecuzione di un algoritmo formalmente 
corretto.
Tuttavia, un programma non si valuta solo in base alla correttezza formale, ma anche in base alle sue prestazioni.
 Per questo motivo in C++ e' possibile definire funzioni che accettano riferimenti alla memoria come parametri,
 pittosto che variabili ordinarie. Questa seconda modalita' viene detta passaggio per riferimento e prevede l'uso
 di parametri di tipo puntatore a variabile o variabili reference.









